function listToTable(list)
  local iter = list
  local acc = {}
  while iter.tail do
    table.insert(acc, iter.head)
    iter = iter.tail
  end
  return acc
end

function cons(val, list)
  return { head = val, tail = list }
end

function mapList(f, list)
  if list.tail then
    return { head = f(list.head), tail = mapList(f, list.tail) }
  else
    return {}
  end
end

function appendList(xs, ys)
  if xs.tail then
    return { head = xs.head, tail = appendList(xs.tail, ys) }
  else
    return ys
  end
end

function reverseTable(t)
  local ret = {}
  for i = #t, 1, -1 do
    table.insert(ret, t[i])
  end
  return ret
end

-- parseLines : String → ([[Int]], [Int])
-- returns parsed lines:
--   - list of list of unicode codepoints for every line but the last
--   - list of unicode codepoints for the last line
function parseLines(txt)
  local stack = {}
  for _, codePoint in utf8.codes(txt) do
    table.insert(stack, codePoint)
  end
  local size = #stack

  local start, go

  -- start : Int → ([Int], [[Int]])
  start = function(idx)
    if idx > size then return {}, {} end

    local cp = stack[idx]
    if cp == utf8.codepoint("\n") then
      return go(idx + 1, {}, cons({}, {}))
    else
      return go(idx + 1, cons(cp, {}), {})
    end
  end

  -- go : Int → [Int] → [[Int]] → ([Int], [[Int]])
  go = function(idx, line, lines)
    if idx > size then return line, lines end

    local cp = stack[idx]
    if cp == utf8.codepoint("\n") then
      return go(idx + 1, {}, cons(line, lines))
    else
      return go(idx + 1, cons(cp, line), lines)
    end
  end

  local last, lines = start(1)
  return
    reverseTable(listToTable(mapList(
      function(line)
        return reverseTable(listToTable(line))
      end,
      lines
    ))),
    reverseTable(listToTable(last))
end

local oparen = utf8.codepoint("(")
local cparen = utf8.codepoint(")")
local obrace = utf8.codepoint("{")
local cbrace = utf8.codepoint("}")
local nl = utf8.codepoint("\n")
local space = utf8.codepoint(" ")
local tab = utf8.codepoint("\t")
local isSpace = function(cp) return cp == space or cp == tab end
local dollar = utf8.codepoint("$")
local at = utf8.codepoint("@")
local hash = utf8.codepoint("#")
local isSymbol = function(cp)
  return cp == dollar or cp == at or cp == hash
end
local isCBracket = function(cp)
  return cp == cparen or cp == cbrace
end
local isOBracket = function(cp)
  return cp == oparen or cp == obrace
end
-- codepoints to string
local cpsToString = function(cps)
  return utf8.char(table.unpack(listToTable(cps)))
end
local function matchingBracket(char)
  if char == "(" then return ")"
  elseif char == ")" then return "("
  elseif char == "{" then return "}"
  else return "{"
  end
end

-- parse a line (as a table of utf8 codepoints, no newlines) into a
-- standalone reference padded by whitespaces or a table
-- of references (table of text blocks and identifiers).
--
-- parseLine : [Int] →
-- { padded : {
--     lead: String,
--     symbol: String,
--     bracket: String,
--     id: String,
--     trail: String
--   }
-- | references: [
--     { reference: {
--         symbol: String,
--         symbol: String,
--         bracket: String,
--         id: String
--       }
--     | text: String
--     }
--   ]
-- }
function parseRefLine(line)
  -- parsing in reverse - from right to left
  local lineRev = reverseTable(line)
  local size = #lineRev

  -- parsing states
  local start,
        pad0, pad1, pad2, pad3, pad4, pad5,
        ref0, ref1, ref2, ref3, ref4

  -- ... $
  start = function(idx)
    if idx > size then
      return { references = {} }
    end

    local cp = lineRev[idx]
    if isSpace(cp) then
      return pad0(idx + 1, cons(cp, {}))
    elseif isCBracket(cp) then
      return pad1(idx + 1, {}, cp)
    else
      return ref0(idx + 1, {}, cons(cp, {}))
    end
  end

  -- ... \w* $
  ref0 = function(idx, refs, trail)
    if idx > size then
      return { references = cons({ text = trail }, refs) }
    end

    local cp = lineRev[idx]
    if isCBracket(cp) then
      return ref1(idx + 1, refs, trail, cp)
    else
      return ref0(idx + 1, refs, cons(cp, trail))
    end
  end

  -- ... ) \w* $
  ref1 = function(idx, refs, trail, cbracket)
    if idx > size then
      return { references = cons({ text = cons(cbracket, trail) }, refs) }
    end

    local cp = lineRev[idx]
    if isSpace(cp) or isOBracket(cp) then
      return ref0(idx + 1, refs, cons(cp, cons(cbracket, trail)))
    elseif isCBracket(cp) then
      return ref1(idx + 1, refs, cons(cbracket, trail), cp)
    else
      return ref2(idx + 1, refs, trail, cbracket, cons(cp, {}))
    end
  end

  -- ... \w* ) \w* $
  ref2 = function(idx, refs, trail, cbracket, id)
    if idx > size then
      return {
        references = cons({ text = appendList(id, cons(cbracket, trail)) }, refs)
      }
    end

    local cp = lineRev[idx]
    if isSpace(cp) then
      return ref0(idx + 1, refs, cons(cp, appendList(id, cons(cbracket, trail))))
    elseif isCBracket(cp) then
      return ref1(idx + 1, refs, appendList(id, cons(cbracket, trail)), cp)
    elseif (cp == oparen and cbracket == cparen) or
           (cp == obrace and cbracket == cbrace) then
      return ref3(idx + 1, refs, trail, cbracket, id, cp)
    else
      return ref2(idx + 1, refs, trail, cbracket, cons(cp, id))
    end
  end

  -- ... ( \w* ) \w* $
  ref3 = function(idx, refs, trail, cbracket, id, obracket)
    if idx > size then
      return {
        references = cons(
          { text = cons(obracket, appendList(id, cons(cbracket, trail))) },
          refs
        )
      }
    end

    local cp = lineRev[idx]
    if isCBracket(cp) then
      return ref1(idx + 1, refs, cons(obracket, appendList(id, cons(cbracket, trail))), cp)
    elseif isSymbol(cp) then
      return ref4(idx + 1, refs, trail, cbracket, id, obracket, cp)
    else
      return ref0(
        idx + 1,
        refs,
        cons(cp, cons(obracket, appendList(id, cons(cbracket, trail))))
      )
    end
  end

  -- ... $ ( \w* ) \w* $
  ref4 = function(idx, refs, trail, cbracket, id, obracket, symbol)
    if idx > size then
      return {
        references = cons(
          { reference = { id = id, symbol = symbol, bracket = obracket } },
          cons({ text = trail }, refs)
        )
      }
    end

    local cp = lineRev[idx]
    if isCBracket(cp) then
      return ref1(
        idx + 1,
        cons(
          { reference = { id = id, symbol = symbol, bracket = obracket } },
          cons({ text = trail }, refs)
        ),
        {},
        cp
      )
    elseif cp == symbol then
      return ref0(
        idx + 1,
        refs,
        cons(symbol, cons(obracket, appendList(id, cons(cbracket, trail))))
      )
    else
      return ref0(
        idx + 1,
        cons(
          { reference = { id = id, symbol = symbol, bracket = obracket } },
          cons({ text = trail }, refs)
        ),
        cons(cp, {})
      )
    end
  end

  -- ... \s* $
  pad0 = function(idx, trail)
    if idx > size then
      return { references = cons({ text = trail }, {}) }
    end

    local cp = lineRev[idx]
    if isCBracket(cp) then
      return pad1(idx + 1, trail, cp)
    elseif isSpace(cp) then
      return pad0(idx + 1, cons(cp, trail))
    else
      return ref0(idx + 1, {}, cons(cp, trail))
    end
  end

  -- ... ) \s* $
  pad1 = function(idx, trail, cbracket)
    if idx > size then
      return { references = cons({ text = cons(cbracket, trail) }, {}) }
    end

    local cp = lineRev[idx]
    if isSpace(cp) or isOBracket(cp) then
      return ref0(idx + 1, {}, cons(cp, cons(cbracket, trail)))
    elseif isCBracket(cp) then
      return ref1(idx + 1, {}, cons(cbracket, trail), cp)
    else
      return pad2(idx + 1, trail, cbracket, cons(cp, {}))
    end
  end

  -- ... \w* ) \s* $
  pad2 = function(idx, trail, cbracket, id)
    if idx > size then
      return { references = cons({ text = appendList(id, cons(cbracket, trail)) }, {}) }
    end

    local cp = lineRev[idx]
    if isCBracket(cp) then
      return ref1(idx + 1, {}, appendList(id, cons(cbracket, trail)), cp)
    elseif (cp == oparen and cbracket == cparen) or
           (cp == obrace and cbracket == cbrace) then
      return pad3(idx + 1, trail, cbracket, id, cp)
    elseif isSpace(cp) then
      return ref0(idx + 1, {}, cons(cp, appendList(id, cons(cbracket, trail))))
    else
      return pad2(idx + 1, trail, cbracket, cons(cp, id))
    end
  end

  -- ... ( \w* ) \s* $
  pad3 = function(idx, trail, cbracket, id, obracket)
    if idx > size then
      return {
        references = cons(
          { text = cons(obracket, appendList(id, cons(cbracket, trail))) },
          {}
        )
      }
    end

    local cp = lineRev[idx]
    if isSymbol(cp) then
      return pad4(idx + 1, trail, cbracket, id, obracket, cp)
    elseif isCBracket(cp) then
      return ref1(
        idx + 1,
        {},
        cons(obracket, appendList(id, cons(cbracket, trail))),
        cp
      )
    else
      return ref0(
        idx + 1,
        {},
        cons(cp, cons(obracket, appendList(id, cons(cbracket, trail))))
      )
    end
  end

  -- ... $ ( \w* ) \s* $
  pad4 = function(idx, trail, cbracket, id, obracket, symbol)
    if idx > size then
      return {
        padded = {
          lead = {},
          symbol = symbol,
          bracket = obracket,
          id = id,
          trail = trail
        }
      }
    end

    local cp = lineRev[idx]
    if isSpace(cp) then
      return pad5(idx + 1, trail, cbracket, id, obracket, symbol, cons(cp, {}))
    elseif isCBracket(cp) then
      return ref1(
        idx + 1,
        cons(
          { reference = { id = id, symbol = symbol, bracket = obracket } },
          cons({ text = trail }, {})
        ),
        {},
        cp
      )
    elseif cp == symbol then
      return ref0(
        idx + 1,
        cons(
          { text = cons(symbol, cons(obracket, appendList(id, cons(cbracket, trail)))) },
          {}
        ),
        {}
      )
    else
      return ref0(
        idx + 1,
        cons(
          { reference = { id = id, symbol = symbol, bracket = obracket } },
          cons({ text = trail }, {})
        ),
        cons(cp, {})
      )
    end
  end

  -- ... \s* $ ( \w* ) \s* $
  pad5 = function(idx, trail, cbracket, id, obracket, symbol, lead)
    if idx > size then
      return {
        padded = {
          lead = lead,
          symbol = symbol,
          bracket = obracket,
          id = id,
          trail = trail
        }
      }
    end

    local cp = lineRev[idx]
    if isSpace(cp) then
      return pad5(idx + 1, trail, cbracket, id, obracket, symbol, cons(cp, lead))
    elseif isCBracket(cp) then
      return ref1(
        idx + 1,
        cons(
          { reference = { id = id, symbol = symbol, bracket = obracket } },
          cons({ text = trail }, {})
        ),
        lead,
        cp
      )
    else
      return ref0(
        idx + 1,
        cons(
          { reference = { id = id, symbol = symbol, bracket = obracket } },
          cons({ text = trail }, {})
        ),
        cons(cp, lead)
      )
    end
  end

  local parsedLists = start(1)
  if parsedLists.references then
    return {
      references = listToTable(
        mapList(
          function(r)
            if r.reference then
              return {
                reference = {
                  symbol = utf8.char(r.reference.symbol),
                  bracket = utf8.char(r.reference.bracket),
                  id = cpsToString(r.reference.id)
                }
              }
            elseif r.text then
              return { text = cpsToString(r.text) }
            else
              error("impossible")
            end
          end,
          parsedLists.references
        )
      )
    }
  elseif parsedLists.padded then
    return {
      padded = {
        lead = cpsToString(parsedLists.padded.lead),
        symbol = utf8.char(parsedLists.padded.symbol),
        bracket = utf8.char(parsedLists.padded.bracket),
        id = cpsToString(parsedLists.padded.id),
        trail = cpsToString(parsedLists.padded.trail)
      }
    }
  else
    error "impossible"
  end
end

function parseRefs(txt)
  local lines, last = parseLines(txt)
  local ret = {}
  local function onLine(line)
    local parsed = parseRefLine(line)
    if parsed.references then
      for _, r in pairs(parsed.references) do
        if r.reference then
          table.insert(ret, {
            typ = "ref",
            symbol = r.reference.symbol,
            bracket = r.reference.bracket,
            id = r.reference.id
          })
        elseif r.text then
          if r.text ~= "" then
            table.insert(ret, {
              typ = "text",
              text = r.text
            })
          end
        else error "impossible"
        end
      end
    elseif parsed.padded then
      table.insert(ret, {
        typ = "pad",
        lead = parsed.padded.lead,
        symbol = parsed.padded.symbol,
        bracket = parsed.padded.bracket,
        id = parsed.padded.id,
        trail = parsed.padded.trail
      })
    else error "impossible"
    end
  end
  for _, line in pairs(lines) do
    onLine(line)
    table.insert(ret, {
      typ = "newline",
    })
  end
  onLine(last)
  return ret
end

local function testParse()
  local function parse(str)
    local acc = {}
    for _, v in pairs(parseRefs(str)) do
      if v.typ == "pad" then
        table.insert(
          acc,
          "p|" .. v.lead .. "|" .. v.symbol .. v.bracket .. v.id .. "|" .. v.trail
        )
      elseif v.typ == "ref" then
        table.insert(acc, "r|" .. v.symbol .. v.bracket .. v.id)
      elseif v.typ == "text" then
        table.insert(acc, "t|" .. v.text)
      elseif v.typ == "newline" then
        table.insert(acc, "n")
      else error "impossible"
      end
    end
    return acc
  end

  local function assertParse(input, output)
    for i, k in ipairs(parse(input)) do
      assert(k == output[i])
    end
  end

  assertParse("", {})
  assertParse(" ", {"t| "})
  assertParse("a", {"t|a"})
  assertParse("(", {"t|("})
  assertParse("{", {"t|{"})
  assertParse("$", {"t|$"})
  assertParse(")", {"t|)"})
  assertParse("}", {"t|}"})
  assertParse(" a", {"t| a"})
  assertParse("aa", {"t|aa"})
  assertParse("(a", {"t|(a"})
  assertParse("{a", {"t|{a"})
  assertParse("$a", {"t|$a"})
  assertParse(")a", {"t|)a"})
  assertParse("}a", {"t|}a"})
  assertParse(" )a", {"t| )a"})
  assertParse("a)a", {"t|a)a"})
  assertParse("()a", {"t|()a"})
  assertParse("{)a", {"t|{)a"})
  assertParse("$)a", {"t|$)a"})
  assertParse("))a", {"t|))a"})
  assertParse("})a", {"t|})a"})
  assertParse(" a)a", {"t| a)a"})
  assertParse("aa)a", {"t|aa)a"})
  assertParse("(a)a", {"t|(a)a"})
  assertParse("{a)a", {"t|{a)a"})
  assertParse("$a)a", {"t|$a)a"})
  assertParse(")a)a", {"t|)a)a"})
  assertParse("}a)a", {"t|}a)a"})
  assertParse(" (a)a", {"t| (a)a"})
  assertParse("a(a)a", {"t|a(a)a"})
  assertParse("((a)a", {"t|((a)a"})
  assertParse("{(a)a", {"t|{(a)a"})
  assertParse("$(a)a", {"r|$(a", "t|a"})
  assertParse(")(a)a", {"t|)(a)a"})
  assertParse("}(a)a", {"t|}(a)a"})
  assertParse(" $(a)a", {"t| ", "r|$(a", "t|a"})
  assertParse("a$(a)a", {"t|a", "r|$(a", "t|a"})
  assertParse("($(a)a", {"t|(", "r|$(a", "t|a"})
  assertParse("{$(a)a", {"t|{", "r|$(a", "t|a"})
  assertParse("$$(a)a", {"t|$(a)a"})
  assertParse(")$(a)a", {"t|)", "r|$(a", "t|a"})
  assertParse("}$(a)a", {"t|}", "r|$(a", "t|a"})
  assertParse("  ", {"t|  "})
  assertParse("a ", {"t|a "})
  assertParse("( ", {"t|( "})
  assertParse("{ ", {"t|{ "})
  assertParse("$ ", {"t|$ "})
  assertParse(") ", {"t|) "})
  assertParse("} ", {"t|} "})
  assertParse(" ) ", {"t| ) "})
  assertParse("a) ", {"t|a) "})
  assertParse("() ", {"t|() "})
  assertParse("{) ", {"t|{) "})
  assertParse("$) ", {"t|$) "})
  assertParse(")) ", {"t|)) "})
  assertParse("}) ", {"t|}) "})
  assertParse(" a) ", {"t| a) "})
  assertParse("aa) ", {"t|aa) "})
  assertParse("(a) ", {"t|(a) "})
  assertParse("{a) ", {"t|{a) "})
  assertParse("$a) ", {"t|$a) "})
  assertParse(")a) ", {"t|)a) "})
  assertParse("}a) ", {"t|}a) "})
  assertParse(" (a) ", {"t| (a) "})
  assertParse("a(a) ", {"t|a(a) "})
  assertParse("((a) ", {"t|((a) "})
  assertParse("{(a) ", {"t|{(a) "})
  assertParse("$(a) ", {"p||$(a| "})
  assertParse(")(a) ", {"t|)(a) "})
  assertParse("}(a) ", {"t|}(a) "})
  assertParse(" $(a) ", {"p| |$(a| "})
  assertParse("a$(a) ", {"t|a", "r|$(a", "t| "})
  assertParse("($(a) ", {"t|(", "r|$(a", "t| "})
  assertParse("{$(a) ", {"t|{", "r|$(a", "t| "})
  assertParse("$$(a) ", {"t|$(a) "})
  assertParse(")$(a) ", {"t|)", "r|$(a", "t| "})
  assertParse("}$(a) ", {"t|}", "r|$(a", "t| "})
  assertParse("  $(a) ", {"p|  |$(a| "})
  assertParse("a $(a) ", {"t|a ", "r|$(a", "t| "})
  assertParse("( $(a) ", {"t|( ", "r|$(a", "t| "})
  assertParse("{ $(a) ", {"t|{ ", "r|$(a", "t| "})
  assertParse("$ $(a) ", {"t|$ ", "r|$(a", "t| "})
  assertParse(") $(a) ", {"t|) ", "r|$(a", "t| "})
  assertParse("} $(a) ", {"t|} ", "r|$(a", "t| "})
end

local function encodeUft8(txt)
  local acc = {}
  for _, codePoint in utf8.codes(txt) do
    if codePoint > 0x10ffff then
      error("Unicode codepoint greater then 0x10ffff")
    end
    if codePoint <= 0x7f then
      table.insert(acc, codePoint)
    elseif codePoint <= 0x7ff then
      table.insert(acc, 0xc0 | (codePoint >> 6))
      table.insert(acc, 0x80 | (0x3f & codePoint))
    elseif codePoint <= 0xffff then
      table.insert(acc, 0xe0 | (codePoint >> 12))
      table.insert(acc, 0x80 | (0x3f & (codePoint >> 6)))
      table.insert(acc, 0x80 | (0x3f & codePoint))
    else
      table.insert(acc, 0xf0 | (codePoint >> 18))
      table.insert(acc, 0x80 | (0x3f & (codePoint >> 12)))
      table.insert(acc, 0x80 | (0x3f & (codePoint >> 6)))
      table.insert(acc, 0x80 | (0x3f & codePoint))
    end
  end
  return acc
end

return {
  -- Substitute references for tangle blocks
  {
    Pandoc = function(doc)

      local codeBlockText = {}
      doc:walk {
        CodeBlock = function(e)
          if e.attr.identifier ~= "" then
            codeBlockText[e.attr.identifier] = e.text
          end
        end
      }

      local substitutedBlocks = {}
      local visitingBlocks = {}
      local substitute, onRef
      onRef = function(chunk)
        local id = chunk.id
        local doneText
        for _, visitedId in pairs(visitingBlocks) do
          if id == visitedId then
            error(
              "\"" .. visitedId .. "\" is part of the cycle: " ..
              table.concat(visitingBlocks, ", ")
            )
          end
        end
        if substitutedBlocks[id] then
          doneText = substitutedBlocks[id]
        else
          table.insert(visitingBlocks, id)
          doneText = substitute(codeBlockText[id])
          visitingBlocks[#visitingBlocks] = nil
          substitutedBlocks[id] = doneText
        end
        if chunk.bracket == "(" then
          doneText = load(doneText)
          if not doneText then
            error "referred block is not valid lua"
          end
          doneText = doneText()
        end
        return doneText
      end
      substitute = function(txt)
        local ret = {}
        for _, chunk in pairs(parseRefs(txt)) do
          if chunk.typ == "pad" and chunk.symbol == "$" then
            if not codeBlockText[chunk.id] then -- unknown id
              table.insert(ret, chunk.lead)
              table.insert(ret, chunk.symbol)
              table.insert(ret, chunk.bracket)
              table.insert(ret, chunk.id)
              table.insert(ret, matchingBracket(chunk.bracket))
              table.insert(ret, chunk.trail)
            else
              local substitutedText = onRef(chunk)
              local lines, last = parseLines(substitutedText)
              for _, line in pairs(lines) do
                table.insert(ret, chunk.lead)
                table.insert(ret, utf8.char(table.unpack(line)))
                table.insert(ret, "\n")
              end
              table.insert(ret, chunk.lead)
              table.insert(ret, utf8.char(table.unpack(last)))
            end
          elseif (chunk.typ == "ref" and chunk.symbol == "$") then
            error "In line reference can only be surrounded by spaces."
          elseif (chunk.typ == "pad" and not chunk.symbol == "$") or
                 chunk.typ == "ref" then
            if not codeBlockText[chunk.id] then -- unknown id
              table.insert(ret, chunk.symbol)
              table.insert(ret, chunk.bracket)
              table.insert(ret, chunk.id)
              table.insert(ret, matchingBracket(chunk.bracket))
            else
              local substitutedText = onRef(chunk)
              if chunk.symbol == "@" then
                table.insert(ret, table.concat(encodeUft8(substitutedText), ", "))
              elseif chunk.symbol == "#" then
                table.insert(ret, table.concat(encodeUft8(substitutedText), " "))
              else error("unknown symbol \"" .. chunk.symbol .. "\"")
              end
            end
          elseif chunk.typ == "text" then
            table.insert(ret, chunk.text)
          elseif chunk.typ == "newline" then
            table.insert(ret, "\n")
          else error "impossible"
          end
        end
        return table.concat(ret)
      end

      doc:walk {
        CodeBlock = function(e)
          for _, class in pairs(e.attr.classes) do
            if class == "tangle" then
              local codeBlockAction = load(substitute(e.text))
              if not codeBlockAction then
                error "tangle block is not valid lua"
              end
              codeBlockAction()
              break
            end
          end
        end
      }
    end
  },
  {
    CodeBlock = function(elem)
      local acc = {}
      local references = {}
      local symbols = {}
      local brackets = {}

      local function onId(id, symbol, bracket)
        local sha = pandoc.sha1(id)
        references[sha] = id
        symbols[sha] = symbol
        brackets[sha] = bracket
        return sha
      end

      for _, chunk in pairs(parseRefs(elem.text)) do
        if chunk.typ == "pad" and chunk.symbol == "$" then
          table.insert(acc, chunk.lead)
          table.insert(acc, "a")
          table.insert(acc, onId(chunk.id, chunk.symbol, chunk.bracket))
          table.insert(acc, chunk.trail)
        elseif (chunk.typ == "ref" and chunk.symbol == "$") then
          error "In line reference can only be surrounded by spaces."
        elseif (chunk.typ == "pad" and not chunk.symbol == "$") or
               chunk.typ == "ref" then
          table.insert(acc, "a")
          table.insert(acc, onId(chunk.id, chunk.symbol, chunk.bracket))
        elseif chunk.typ == "text" then
          table.insert(acc, chunk.text)
        elseif chunk.typ == "newline" then
          table.insert(acc, "\n")
        else error "impossible"
        end
      end

      elem.text = table.concat(acc)
      local html = pandoc.write(
        pandoc.Pandoc(elem),
        "html"
      )
      for sha, id in pairs(references) do
        local symbol = symbols[sha]
        if symbol == "%" then symbol = "%%" end
        html = string.gsub(
          html,
          "a" .. sha,
          "<a href=#" .. id
                      .. ">"
                      .. symbol
                      .. brackets[sha]
                      .. id
                      .. matchingBracket(brackets[sha])
                      .. "</a>"
        )
      end

      -- wrapping inside a div with a dummy CodeBlock
      -- to regain possibly lost highlighting
      local dummy = pandoc.CodeBlock ""
      dummy.attr.classes = elem.attr.classes
      dummy.attributes.style = "display: none"
      local wrap = pandoc.Div { dummy, pandoc.RawBlock("html", html) }

      return wrap
    end
  }
}
