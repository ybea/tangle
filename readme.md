# tangle - literate programming with pandoc

## Introduction

[Literate programming](https://en.wikipedia.org/wiki/Literate_programming)
is a way of writing a program together with its
documentation. Usually the documentation (prose) and code fragments are
put in a single file. The process of extracting documentation into its
separate format (for example `html`) is called weaving. Tangling, on
the other hand extracts the program (or programs).

This tool is motivated by
[Why all you'll ever need is markdown](https://blog.esciencecenter.nl/why-all-youll-ever-need-is-markdown-dc604f0ab309)
and requires `pandoc` to do all weaving. By itself it only
provides minimal set of constructs to enable tangling. It operates
on fenced code blocks in a markdown document.

### Features

- No dependencies other than `pandoc`. It's a lua filter.
- Unicode support
- Language agnostic
- References with cycle detection

### Example

`example.md`:
````markdown
# Hello world

## Overview

Hello world program in `C`:
```{ .c #hello-world }
${includes}

${main}
```

## Includes

We only need `stdio.h`:
```{ .c #includes }
#include <stdio.h>
```

## Main

Finally the `main` function:
```{ .c #main }
int main(int argc, char **argv) {
  printf("Hello world\n");
}
```

## Building:

```{ .lua .tangle }
local file = io.output("example.c")
file:write(string.char(table.unpack({@{hello-world}})))
file:close()
os.execute("gcc example.c -o example")
```
````

Now run `pandoc`:
```bash
pandoc -s --lua-filter=tangle.lua example.md > example.html
```

During the run of `pandoc`, blocks with class `.tangle` will be
executed. Here, last block referes to the first one. Resulting
`example.html` for this block will have a link to it,
but the block itself will see utf-8 encoded bytes for the referred
block, so they can be saved to a file and compiled by `gcc`.

## Overview

There are two ways of controlling how references appear in
the surrounding block. Preceging symbol controls if a block
should be embedded literally or as a list of UTF8 bytes.
Type of brackets determine if an input block should
be treated as string or as lua code that returns the string.

Identifier inside the brackets names the referred block,
which should be marked with regular `pandoc` syntax.

### Dollar sign

References preceded with `$` are fully expanded to other blocks
(if surrounded with curly braces or return strings after evaluating them
if surrounded with parentheses).
````markdown
```{ #fst }
A
B
```

```{ #snd }
return "E\nF"
```

```
  ${fst}
  C
  D
  $(snd)
```
````

Here A, B, C, D, E, F will be all lined up, because whitespaces before
the links will be copied before each line of the referred block
and lua outputs. This feature also restricts dollar references from appearing
alone in a line.

````markdown
! BAD CODE !

``` { #id }
17
```
```
f = ${id}
```
````

### At sign

Preceding a reference with `@` makes it embedded as a list of number literals
separated by a comma, representing block's (or lua code output, if it's round
brackets) UTF8 encoding.

For languages that don't allow comma between numbers (e.g. `scheme`), `#`
can be used. Resulting numbers are separated by a single space.

````markdown
``` { #str }
😀 ★ Some string ★ 😀
```

``` { .c #c-example }
#include <stdio.h>

int main() {
  char str[] = {@{str}, 0};
  printf("%s\n", str);
}
```

``` { .javascript #js-example }
var decoder = new TextDecoder('utf-8')
var bytes = new Uint8Array([@{str}])
console.log(decoder.decode(bytes))
```

``` { .scheme #scm-example }
(use-modules (rnrs bytevectors))

(display
  (utf8->string
    (u8-list->bytevector
      '(#{str}))))
```

``` { .haskell #hs-example }
import qualified Data.ByteString as BS
import qualified Data.Text.Encoding as Text
import qualified Data.Text.IO as Text

main :: IO ()
main =
  Text.putStrLn $
    Text.decodeUtf8 $
      BS.pack [@{str}]
```

```{ .lua .tangle }
local file

file = io.output("example.c")
file:write(string.char(table.unpack({@{c-example}})))
file:close()

file = io.output("example.js")
file:write(string.char(table.unpack({@{js-example}})))
file:close()

file = io.output("example.scm")
file:write(string.char(table.unpack({@{scm-example}})))
file:close()

file = io.output("example.hs")
file:write(string.char(table.unpack({@{hs-example}})))
file:close()
```
````

Note that expanded numbers are UTF8 bytes, not Unicode codepoints. In `lua`
block it would be incorrect to put `utf8.char` instead of `string.char`.

### Escaping

Inside a code block, if a character preceding the paretheses is doubled,
then the text does not refer to other blocks and is treated literally.

````markdown
```
@@(these) $${are} ##{not} $$(links)
```
````

appears in `html` as `@(these) ${are} #{not} $(links)`.

## Missing features / possible improvements

### Line Directives

If a code block is submitted to compilation and it fails, the errors
point to lines of a generated block, instead of the source file (`.md`).
Some compilers give an option to override this information with
line directives. Unfortunately `pandoc` doesn't pass line numbers
of parsed code blocks, so any errors are reported with wrong origin.

### Extending existing blocks

`Noweb` tools often expose `<< >>+` operator, that allows adding text to
already existing block. Not being able to append, forces
writing little TOCs for other blocks. Not necessary a bad thing, but tedious.

Perhaps possible by marking a block with another class.

### Include

Referring and including blocks from other literate files would take
this tool from managing single to multiple-file projects. A substantial
complexity, besides, markdown already provides some hierarchy (`pandoc --toc`).

For including other files, use lua blocks.

### Latex output

Adding anchor links to code makes it a `RawBlock` - specific output (`html`).
It shouldn't be too hard to at least add a case for Latex.

## Notable prior work

As of 2022. These are often very small, sharp programs
that shouldn't be considered abandoned, but rather finished.

### Noweb

- [noweb](https://www.cs.tufts.edu/~nr/noweb/)
- [entangled](https://entangled.github.io/)
  - bidirectional daemon
  - [example output](https://entangled.github.io/examples/plotly.html)
    ([source](https://github.com/entangled/examples/blob/master/lit/plotly.md))
- [lipsum](https://github.com/lindig/lipsum)
  - `noweb` as a single binary
  - not extensible
- [lit](https://github.com/cdosborn/lit)
  - minimalistic
  - self contained
  - clean
  - only two constructs

### CWEB

Tools that can operate not only on a single literate file, but on a "web" of
files and references.

- [Literate](https://zyedidia.github.io/literate/)
- [inweb](https://github.com/ganelson/inweb)

### Other Markdown tools

- [literatex](https://github.com/ExtremaIS/literatex-haskell)
  - Extracts markdown documentation from source comments
- [inliterate](https://github.com/diffusionkinetics/open/tree/master/inliterate)
  - [example](https://glutamate.github.io/plotlyhs/)
    ([source](https://github.com/diffusionkinetics/open/blob/master/plotlyhs/gendoc/GenDocInlit.hs))
  - Haskell specific - a preprocessor for GHC
  - Produces a program that when run produces html with
    intertwined haskell blocks (sources or outputs)

#### Extract Markdown code blocks

- [lit](https://github.com/vijithassar/lit)
  - based on AWK oneliner
- [blaze](https://github.com/0atman/blaze)
  - Same as `lit`, but executes tangled program according to specified shebang.
- [mdsh](https://github.com/bashup/mdsh)
  - Runs code blocks and inserts their outputs into Markdown

### Quick and dirty generators

Most of these tools don't support referencing and the order of appearance
in the source is reflected in the output.

- [docco](https://ashkenas.com/docco/)
  (CoffeeScript, [source](https://github.com/jashkenas/docco))
- [docco-next](https://mobily-enterprises.github.io/docco-next/)
  (ES6, [source](https://github.com/mobily-enterprises/docco-next))
- [groc](https://nevir.github.io/groc/)
  (CoffeeScript, [source](https://github.com/nevir/groc))
  - Searchable TOC
  - Supports source hierarchies
- [rocco](https://rtomayko.github.io/rocco/rocco.html)
  (Ruby, [source](https://github.com/rtomayko/rocco))
- [pycco](https://pycco-docs.github.io/pycco/)
  (Python, [source](https://github.com/pycco-docs/pycco))
- [marginalia](https://gdeer81.github.io/marginalia/)
  (Clojure, [source](https://github.com/gdeer81/marginalia))
- [shocco](https://rtomayko.github.io/shocco/)
  (Shell, [source](https://github.com/rtomayko/shocco))
- [nocco](http://donwilson.net/nocco/)
  (C#, [source](https://github.com/dontangg/nocco))
- [locco](https://rgieseke.github.io/locco/)
  (Lua, [source](https://github.com/rgieseke/locco))
- [hyacco](https://sourrust.github.io/hyakko/)
  (Haskell, [source](https://github.com/sourrust/hyakko))
